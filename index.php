<link rel="stylesheet" href="/markdown/theme/style.css">

<?php

ini_set('include_path', ini_get('include_path')
                        . PATH_SEPARATOR . __DIR__ . DIRECTORY_SEPARATOR . 'lib') ;

require_once 'Michelf/MarkdownExtra.inc.php';

$strFile = substr($_SERVER['REQUEST_URI'], strlen(dirname($_SERVER['SCRIPT_NAME'])));

$strFile = $strFile != '/' ? $strFile : $strFile . 'Home.md' ;

$strFile = 'wiki' . $strFile ;

if ( ! file_exists($strFile)) 
{
    header('HTTP/1.0 404 Not Found') ;
    
    exit(0) ;
}

use \Michelf\MarkdownExtra;
$output = MarkdownExtra::defaultTransform(file_get_contents($strFile));

echo $output ;
